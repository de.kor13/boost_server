#include <session.h>

void Session::start() {
  room_.join(shared_from_this());

  boost::asio::async_read(socket_,
    boost::asio::buffer(read_msg_.data(), message::header_length_arg1),
    boost::bind( &
      Session::handle_read_header, shared_from_this(),
      boost::asio::placeholders::error));

}

void Session::deliver(const message & msg) {
  bool write_in_progress = !write_msgs_.empty();
  write_msgs_.push_back(msg);
  if (!write_in_progress) {
    boost::asio::async_write(socket_,
      boost::asio::buffer(write_msgs_.front().data(),
        write_msgs_.front().length()),
      boost::bind( & Session::handle_write, shared_from_this(),
        boost::asio::placeholders::error));
  }
}

void Session::handle_read_header(const boost::system::error_code & error) {

  if (!error && read_msg_.decode_header_first()) {
    boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.body_arg1(), read_msg_.body_length_arg1()),
      boost::bind( & Session::handle_read_body, shared_from_this(),
        boost::asio::placeholders::error));

    char str_arg1[read_msg_.body_length_arg1()];
    memcpy(str_arg1, read_msg_.body_arg1(), read_msg_.body_length_arg1()); //+1
    read_args_.first = str_arg1;

  } else {
    room_.leave(shared_from_this());
  }
}

void Session::handle_read_body(const boost::system::error_code & error) {
  if (!error) {
    boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.data() + message::header_length_arg1 + read_msg_.body_length_arg1(), message::header_length_arg2),
      boost::bind( & Session::handle_read_header_arg2, shared_from_this(),
        boost::asio::placeholders::error));
  } else {
    room_.leave(shared_from_this());
  }
}

void Session::handle_read_header_arg2(const boost::system::error_code & error) {
  if (!error && read_msg_.decode_header_second()) {
    boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.body_arg2(), read_msg_.body_length_arg2()),
      boost::bind( & Session::handle_read_body_arg2, shared_from_this(),
        boost::asio::placeholders::error));

  } else {
    room_.leave(shared_from_this());
  }
}

void Session::handle_read_body_arg2(const boost::system::error_code & error) {
  if (!error) {
    boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.data(), message::header_length_arg1),
      boost::bind( & Session::handle_read_header, shared_from_this(),
        boost::asio::placeholders::error));

    if (read_msg_.body_length_arg2() > 0) {
      char str_arg2[read_msg_.body_length_arg2()];
      strcpy(str_arg2, "");
      memcpy(str_arg2, read_msg_.body_arg2(), read_msg_.body_length_arg2()); //+1
      read_args_.second = str_arg2;
    } else
      read_args_.second = "";

    command_control(read_args_);

    read_args_.first = "";
    read_args_.second = "";
  } else {
    room_.leave(shared_from_this());
  }
}

void Session::handle_write(const boost::system::error_code & error) {
  if (!error) {
    write_msgs_.pop_front();
    if (!write_msgs_.empty()) {
      boost::asio::async_write(socket_,
        boost::asio::buffer(write_msgs_.front().data(),
          write_msgs_.front().length()),
        boost::bind( & Session::handle_write, shared_from_this(),
          boost::asio::placeholders::error));
    }
  } else {
    room_.leave(shared_from_this());
  }
}

void Session::command_control(std::pair < std::string, std::string > & read_args_) {
  message send_msg;

  auto item = msg_map.find(read_args_.first);
  if (item != msg_map.end()) {

    switch (item -> second) {
    case type_msg::GetLedState: {

      send_msg.body_length_arg1(3);
      std::memcpy(send_msg.body_arg1(), "OK", send_msg.body_length_arg1());
      if (led -> getLedState() == 0) {
        send_msg.body_length_arg2(4);
        std::memcpy(send_msg.body_arg2(), "off", send_msg.body_length_arg2());
      } else {
        send_msg.body_length_arg2(3);
        std::memcpy(send_msg.body_arg2(), "on", send_msg.body_length_arg2());
      }
      break;
    }
    case type_msg::SetLedState: {

      std::list < std::string > ::iterator findIter = std::find(led_list.begin(), led_list.end(), read_args_.second);

      if (findIter == led_list.end()) {
        send_msg = faild_msg;
      } else {
        led -> setLedState(read_args_.second);
        send_msg.body_length_arg1(3);
        std::memcpy(send_msg.body_arg1(), "OK", send_msg.body_length_arg1());

        send_msg.body_length_arg2(std::strlen(read_args_.second.c_str()) + 1);

        std::memcpy(send_msg.body_arg2(), read_args_.second.c_str(), send_msg.body_length_arg2());
      }
      break;
    }
    case type_msg::GetLedColor: {

      send_msg.body_length_arg1(3);
      std::memcpy(send_msg.body_arg1(), "OK", send_msg.body_length_arg1());

      send_msg.body_length_arg2(std::strlen(led -> getLedColor().c_str()) + 1);
      std::memcpy(send_msg.body_arg2(), led -> getLedColor().c_str(), send_msg.body_length_arg2());
      break;
    }
    case type_msg::SetLedColor: {

      std::list < std::string > ::iterator findIter = std::find(color_list.begin(), color_list.end(), read_args_.second);

      if (findIter == color_list.end()) {
        send_msg = faild_msg;
      } else {
        led -> setLedColor(read_args_.second);
        send_msg.body_length_arg1(3);
        std::memcpy(send_msg.body_arg1(), "OK", send_msg.body_length_arg1());
        send_msg.body_length_arg2(std::strlen(read_args_.second.c_str()) + 1);
        std::memcpy(send_msg.body_arg2(), read_args_.second.c_str(), send_msg.body_length_arg2());

      }

      break;
    }

    case type_msg::GetLedRate: {

      send_msg.body_length_arg1(3);
      std::memcpy(send_msg.body_arg1(), "OK", send_msg.body_length_arg1());

      send_msg.body_length_arg2(std::strlen(std::to_string(led -> getLedRate()).c_str()) + 1);
      std::memcpy(send_msg.body_arg2(), std::to_string(led -> getLedRate()).c_str(), send_msg.body_length_arg2());
      break;
    }

    case type_msg::SetLedRate: {

      if (atoi(read_args_.second.c_str()) && atoi(read_args_.second.c_str()) > 0 && atoi(read_args_.second.c_str()) <= 5) {
        send_msg.body_length_arg1(3);
        std::memcpy(send_msg.body_arg1(), "OK", send_msg.body_length_arg1());

        led -> setLedRate(atoi(read_args_.second.c_str()));

        send_msg.body_length_arg2(std::strlen(read_args_.second.c_str()) + 1);
        std::memcpy(send_msg.body_arg2(), read_args_.second.c_str(), send_msg.body_length_arg2());

      } else {
        send_msg = faild_msg;
      }

      break;
    }

    }

  }
  send_msg.encode_header();
  this -> deliver(send_msg);

}