
#include <cstdlib>
#include <deque>
#include <set>
#include <list>
#include <unordered_map>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/assign/list_of.hpp> 
#include <boost/asio.hpp>

#include <ledController.h>
#include <message.h>

using namespace boost::asio;
using ip::tcp;
using std::cout;
using std::endl;

enum type_msg {
  SetLedState, // = 'get-led-state/n'
  GetLedState,
  SetLedColor,
  GetLedColor,
  SetLedRate,
  GetLedRate

};

static std::unordered_map < std::string, type_msg > msg_map = {
  {
    "set-led-state",
    SetLedState
  },
  {
    "get-led-state",
    GetLedState
  },
  {
    "set-led-color",
    SetLedColor
  },
  {
    "get-led-color",
    GetLedColor
  },
  {
    "set-led-rate",
    SetLedRate
  },
  {
    "get-led-rate",
    GetLedRate
  },

};

static std::list < std::string > led_list {
  "on",
  "off"
};

static std::list < std::string > color_list {
  "red",
  "green",
  "blue"
};

typedef std::deque < message > client_message_queue;

class client_participant {
  public: virtual~client_participant() {}
  virtual void deliver(const message & msg) = 0;
};

typedef boost::shared_ptr < client_participant > client_participant_ptr;

class Client_room {
  public: void join(client_participant_ptr participant) {
    participants_.insert(participant);
    std::for_each(recent_msgs_.begin(), recent_msgs_.end(),
      boost::bind( & client_participant::deliver, participant, _1));
  }

  void leave(client_participant_ptr participant) {
    participants_.erase(participant);
  }

  void deliver(const message & msg) /*для отправки сообщений всем клиентам, отключино*/ {
    recent_msgs_.push_back(msg);
    while (recent_msgs_.size() > max_recent_msgs)
      recent_msgs_.pop_front();

    std::for_each(participants_.begin(), participants_.end(),
      boost::bind( & client_participant::deliver, _1, boost::ref(msg)));
  }

  private: std::set < client_participant_ptr > participants_;
  enum {
    max_recent_msgs = 100
  };
  client_message_queue recent_msgs_;
};

class Session
  : public client_participant,
  public boost::enable_shared_from_this < Session > {
    public: Session(boost::asio::io_service & io_service, Client_room & room, Led * led_): socket_(io_service),
    room_(room),
    led(led_) {
      faild_msg.body_length_arg1(6);
      std::memcpy(faild_msg.body_arg1(), "FAILD", faild_msg.body_length_arg1());
      faild_msg.body_length_arg2(2);
      std::memcpy(faild_msg.body_arg2(), "!", faild_msg.body_length_arg2());
    }

    tcp::socket & socket() {
      return socket_;
    }

    void start();

    void deliver(const message & msg);

    void handle_read_header(const boost::system::error_code & error);

    void handle_read_body(const boost::system::error_code & error);

    void handle_read_header_arg2(const boost::system::error_code & error);

    void handle_read_body_arg2(const boost::system::error_code & error);

    void handle_write(const boost::system::error_code & error);

    void command_control(std::pair < std::string, std::string > & read_args_);

    private: tcp::socket socket_;
    Client_room & room_;
    message read_msg_;
    message faild_msg;
    client_message_queue write_msgs_;
    std::pair < std::string,
    std::string > read_args_;
    Led * led;

  };