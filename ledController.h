#include <list>
#include <string>

class Led {
  public:

void setLedState(std::string state) {

      if (state == "on")
    _state = true;
    else if (state == "off")
    _state = false;
    else
    _state = false;
};
  void setLedColor(std::string color) {
    _color = color;
  };
  void setLedRate(int rate) {
    _rate = rate;
  };

  bool getLedState() {
    return _state;
  };
  std::string getLedColor() {
    return _color;
  };
  float getLedRate() {
    return _rate;
  };

  private: int _rate = 1; //0...5
  std::string _color = "red"; //green blue
  bool _state = false;

  std::list < std::string > listOfColor = {
    "red",
    "green",
    "blue"
  };

};