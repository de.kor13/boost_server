# Boost_server


## what does the program do?

what does the program do?
This is the server part of the application that controls the video camera.
The server is asynchronous. Multiple client connections available.

By default, connect to localhost:14. To change this, you need to fix the hostname in the code.


## build
CMAKE VERSION 3.15.0
Use boost version 1.71.0

The server listens on port 14, you should check if the port is open.
It is also recommended to use a firewall.
sudo ufw allow 14
sudo ufw reload

mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug -DCPACK_GENERATOR=TGZ -DCMAKE_PREFIX_PATH=/usr/lib && make package
sudo ./server


