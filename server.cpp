
#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <set>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/assign/list_of.hpp> 
#include <boost/asio.hpp>
#include <unordered_map>

#include <session.h>

using namespace boost::asio;
using ip::tcp;

typedef boost::shared_ptr < Session > server_session_ptr;

class Server {
  public: Server(boost::asio::io_service & io_service,
    const tcp::endpoint & endpoint): io_service_(io_service),
  acceptor_(io_service, endpoint)

  {

    server_session_ptr new_session(new Session(io_service_, room_, & led_));
    acceptor_.async_accept(new_session -> socket(),
      boost::bind( & Server::handle_accept, this, new_session,
        boost::asio::placeholders::error));
  }

  void handle_accept(server_session_ptr session,
    const boost::system::error_code & error) {
    if (!error) {
      session -> start();
      server_session_ptr new_session(new Session(io_service_, room_, & led_));
      acceptor_.async_accept(new_session -> socket(),
        boost::bind( & Server::handle_accept, this, new_session,
          boost::asio::placeholders::error));
    }
  }

  private: boost::asio::io_service & io_service_;
  tcp::acceptor acceptor_;
  Client_room room_;
  Led led_;
};

int main() {
  try {
    boost::asio::io_service io_service;
    tcp::endpoint endpoint(tcp::v4(), atoi("14"));
    Server server(io_service, endpoint);
    io_service.run();
  } catch (const std::exception & e) {
    std::cerr << e.what() << endl;
  }
  return 0;
}

